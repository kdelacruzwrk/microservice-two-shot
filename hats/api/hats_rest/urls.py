from django.urls import path

from .views import api_hat_list, api_delete_hat

urlpatterns = [
    path("hats/", api_hat_list, name="api_create_hat"),
    path(
        "locations/<int:location_vo_id>/hats/",
        api_hat_list,
        name="api_hat_list"
    ),
    path("hats/<int:pk>/", api_delete_hat, name="api_delete_hat")
]
