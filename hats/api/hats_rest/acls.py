from .keys import PEXELS_API_KEY
import json
import requests

def get_photo(style_name):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": f"{style_name}"
    }
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    print(content)

    try:
        return {"picture_url": content["photos"][0]["src"]["medium"]}
    except (KeyError, IndexError):
        return {"picture_url": None}
