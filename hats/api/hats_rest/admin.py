from django.contrib import admin
from .models import Hatslist, LocationVO

# Register your models here.

admin.site.register(Hatslist)
admin.site.register(LocationVO)
