from django.db import models
from django.urls import reverse

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200)
    closet_name = models.CharField(max_length=100, null=True)
    shelf_number = models.PositiveSmallIntegerField(null=True)

    def __str__(self):
        return self.closet_name

class Hatslist(models.Model):
    fabric = models.CharField(max_length=100)
    style_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(null=True)
    location = models.ForeignKey(LocationVO, related_name="hatslist", on_delete=models.CASCADE)

    def __str__(self):
        return self.style_name

    def get_api_url(self):
        return reverse("api_delete_hat", kwargs={"pk": self.pk})
