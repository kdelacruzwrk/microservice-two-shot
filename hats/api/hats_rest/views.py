from django.shortcuts import render
from common.json import ModelEncoder
from .models import LocationVO, Hatslist
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .acls import get_photo

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href", "closet_name"]

class HatListEncoder(ModelEncoder):
    model = Hatslist
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "id",
        "location",

    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_hat_list(request, location_vo_href=None,):
    if request.method == "GET":
        if location_vo_href is not None:
            hats = Hatslist.objects.filter(location=location_vo_href)
            print(hats)
        else:
            hats = Hatslist.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder
        )
    else:
        content = json.loads(request.body)
        print(content)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400
            )

        photo = get_photo(content["style_name"])
        content.update(photo)

        location = Hatslist.objects.create(**content)
        return JsonResponse(
            location,
            encoder=HatListEncoder,
            safe=False
        )


@require_http_methods(["DELETE", "GET"])
def api_delete_hat(request, pk):
    if request.method == "GET":
        try:
            hats = Hatslist.objects.get(id=pk)
            return JsonResponse(
                hats,
                encoder=HatListEncoder,
                safe=False
            )
        except Hatslist.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            hats = Hatslist.objects.get(id=pk)
            hats.delete()
            return JsonResponse(
                hats,
                encoder=HatListEncoder,
                safe=False
            )
        except Hatslist.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
