# Wardrobify

Team:

* Mark McTague: Hats
* Kris DelaCruz: Shoes

## How to Run this App

* $docker volume create two-shot-pgdata
* $docker compose build
* $docker compose up

## Diagram

![microservice two shot diagram](/ghi/app/public/diagram.png "Diagram")

## API Documentation

## Shoes API

* api_list_shoes - handles GET and POST requests for shoes
* api_show_shoes - handles GET, DELETE and PUT requests for shoes

## Hats API

* api_hats_list - handles GET and POST requests for hats
* aoi_delete_hats - handles DELETE and GET requests for hats

## Value Objects

* LocationVO - location value object for handling which wardrobe and what location a hat will go in a wardrobe.
* BinVO - bin value object for handling which wardrobe and what bin a pair of shoes will be located in a wardrobe

## URLs and Ports

### Ports

* Hat port 8090:8000
* Shoes port 8080:8000
* Wardrobe port 8100:8000
* React port 3000:3000
* Database port 15432:15432

### Polling urls
* http://wardrobe-api:8000/api/bins/ - Bin/Shoes poller
* http://wardrobe-api:8000/api/locations/ - Location/Hats poller

### Shoes urls

* http://localhost:8080/api/shoes/ - Get a list of details of shoes
* http://localhost:8080/api/shoes/<int:pk> - delete a pair of shoes and details on a pair of shoes

### Hat urls

* http://localhost:8090/api/hats/ - Get a list of hats
* http://localhost:8090/api/locations/<int:location_vo_id>/hats/ - get hats based on location
* http://localhost:8090/api/hats/<int:pk>/ - delete a single hat

### Wardrobe urls

* http://localhost:8090/api/bins/ - Gets a list of all of the bins
* http://localhost:8090/api/bins/<int:pk>/ - Gets the details of one bin
* http://localhost:8090/api/bins/ - Creates a new bin with the posted data
* http://localhost:8090/api/bins/<int:pk>/ - Updates the details of one bin
* http://localhost:8090/api/bins/<int:pk>/ - Deletes a single bin
