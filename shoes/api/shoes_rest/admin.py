from django.contrib import admin
from .models import Shoes, BinVO

# Register your models here.
admin.site.register(Shoes)
admin.site.register(BinVO)
