import React, { useEffect, useState } from "react"

function HatForm() {
const [locations, setLocations] = useState([])
const [style, setStyle] = useState('')
const [fabric, setFabric] = useState('')
const [color, setColor] = useState('')
const [location, setLocation] =useState('')


  const fetchData = async () => {
    const url = "http://localhost:8100/api/locations/"
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json()
      setLocations(data.locations)
    }
  }

  function styleChange(event) {
    setStyle(event.target.value)
  }

  function fabricChange(event) {
    setFabric(event.target.value)
  }

  function colorChange(event) {
    setColor(event.target.value)
  }

  function locationChange(event) {
    setLocation(event.target.value)
  }

  const handleSubmit = async (event) => {
    event.preventDefault()
    const data = {}

    data.style_name = style
    data.fabric = fabric
    data.color = color
    data.location = location

    const hatsUrl = 'http://localhost:8090/api/hats/'
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    }
    const formTag = document.getElementById('create-hat-form')
    const response = await fetch(hatsUrl, fetchConfig)
    console.log(response)
    if (response.ok) {
      formTag.reset()
      const newHat = await response.json()
    }
  }

  useEffect(() => {
    fetchData()
  }, [])
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1> Create a new Hat</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input onChange={styleChange} required placeholder="Style name" type="text"
              name="style_name" id="style_name" className="form-control" />
              <label htmlFor="style_name">Hat Style</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={fabricChange} required placeholder="Style name" type="text"
              name="fabric" id="fabric" className="form-control" />
              <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={colorChange} required placeholder="Style name" type="text"
              name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="mb-3">
              <select onChange={locationChange} required name="location" id="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.href} value={location.href}>{location.closet_name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default HatForm
