import React, { useState, useEffect } from 'react'

function HatsList() {
  const [hatslist, setHatsList] = useState([])

  const getData = async () => {
    const url = 'http://localhost:8090/api/hats/'
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json()
      console.log(data)
      setHatsList(data.hats)
    }
  }

  useEffect(() => {
    getData()
  }, [])

  return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Picture</th>
            <th>Style</th>
            <th>Fabric</th>
            <th>Color</th>
            <th>Wardrobe</th>
          </tr>
        </thead>
        <tbody>
          {hatslist.map(hats => {
            return (
              <tr key={hats.href}>
                <td><img src={hats.picture_url} width='150' height='100' alt='Picture of hat'/></td>
                <td>{hats.style_name}</td>
                <td>{hats.fabric}</td>
                <td>{hats.color}</td>
                <td>{hats.location.closet_name}{' '}
                  <button onClick={() => {
                    setHatsList(
                      hatslist.filter(h =>
                        h.href !== hats.href
                      )
                    )
                  }}>
                    Delete
                  </button>
                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
  )
}

export default HatsList
