import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatForm from './hats/new/HatsForm';
import HatsList from './hats/HatsList';
import ShoesForm from './shoes/new/ShoesForm';
import ShoesList from './shoes/ShoesList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
        </Routes>
        <Routes>
          <Route path='hats'>
            <Route index element={<HatsList />} />
            <Route path='new' element={<HatForm />} />
          </Route>
        </Routes>
        <Routes>
          <Route path='shoes'>
            <Route path='new' element={<ShoesForm />} />
            <Route index element={<ShoesList />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
